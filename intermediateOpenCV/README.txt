Lesson Sequence
1. Basic Video Analysis
2. Color Video Processing (HSV Filters)
    - Class Creation
    - Invoking the Class
3. Face Recognition (Haar Cascades)
    - Class Creation
    - Invoking the Class
4. Haar Cascade Creation
    - Formatting a Positive Image
    - Web Scraping for Negative Images
    - Filtering out Error Images
    - Generating a Description File for Negative Images
    - Commands
        - Creating Positive Image Samples
        - Generating a Positive Image Vector File
        - Training the Haar Cascade
    - Class Creation
    - Invoking the Class