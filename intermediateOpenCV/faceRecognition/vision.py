#This program utilizes the class that was created for face detection
import cv2
import numpy as np
from filters import faceCap

vid = faceCap(0)

while(True):
	vid.collectFrames()
	vid.analyze()
	key = cv2.waitKey(1)
	if(key == ord('q')):
		break
vid.terminate()
