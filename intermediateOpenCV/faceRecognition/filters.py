#This program demonstrates how to set up a class for face detection with Haar cascades
import cv2
import numpy as np

class faceCap():
	#Initializing the stream along with the Haar Cascade
	def __init__(self, camInt):
		self.cap = cv2.VideoCapture(camInt)
		#When loading a pre-made cascade from the OpenCV library, include the prefix 'cv2.data.haarcascades'; this ensures there are no loading issues
		self.cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
	#Collecting Frames from the stream
	def collectFrames(self):
		_, self.frame = self.cap.read()
		#The frame must be grayscale for analysis
		gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
		#'detectMultiScale' requires at least three inputs
		#1. The grayscale frame
		#2. Scale Factor (By how much the frame's size is reduced in scale; in this case by 30%)
		    # The Scale Factor must always be greater than 1; As the factor asymptotically approaches 1, the accuracy increases but the program will run slower
		#3. Minimum Neighbors (The number of neighbors each candidate rectangle in the frame requires in order to be considered a valid feature for detection)
		    # Increasing the number of minimum neighbors reduces the number of false positives, but it's possible the target will not be detected
		self.faces = self.cascade.detectMultiScale(gray, 1.3, 5)
	#Analyzing frames from the stream
	def analyze(self):
		#Each feature detected by the Haar cascade is outputted as a rectangle has starting coordinates, width, and height
		#This loop will cycle through each face that is detected and draw a bounding rectangle; it will also calculate the center coordinate
		for (x,y,w,h) in self.faces:
			self.frame = cv2.rectangle(self.frame, (x,y), (x+w,y+h), (255,255,255), 3)
			ctr = (x+(w/2),y+(h/2))
			print(ctr)
		cv2.imshow('Face', self.frame)
	#Terminating the stream
	def terminate(self):
		self.cap.release()
		cv2.destroyAllWindows()
		
 