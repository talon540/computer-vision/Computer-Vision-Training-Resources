#This program is set up to format any image for use as a 50x50 positive image for cascade training
import cv2
import numpy as np
import sys

img = sys.argv[1]

img = cv2.imread(img)

img = cv2.resize(img, (50,50))

cv2.imwrite('pos.jpg', img)
