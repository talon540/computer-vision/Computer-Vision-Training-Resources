--LESSON SEQUENCE--
1. Formatting Positive Image
2. Web Scraping for Negatives
3. Filtering out Error Images
4. Creating Negative Image Description File
5. OpenCV Commands: Create Positive Image Samples
6. OpenCV Commands: Create Positive Image Vector File
7. OpenCV Commands: Train Haar Cascade