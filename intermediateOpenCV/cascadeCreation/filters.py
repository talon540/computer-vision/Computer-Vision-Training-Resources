#This program demonstrates how to set up a class to use a custom haar cascade
import cv2
import numpy as np

class DetectGear():

	#Initialize the videocapture using a given camera index
	def __init__(self, camInt):
		self.cap = cv2.VideoCapture(camInt)

		#The directory of the cascade file must also be specified
		self.cascade = cv2.CascadeClassifier('data/cascade.xml')
	def collectFrames(self):
		_, self.frame = self.cap.read()
		gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
		self.gears = self.cascade.detectMultiScale(gray, 1.05, 3)
	def analyze(self):
		for (x,y,w,h) in self.gears:
			self.frame = cv2.rectangle(self.frame, (x,y), (x+w,y+h), (255,255,255), 2)
			ctr = (x+(w/2),y+(h/2))
			#print(ctr)
		cv2.imshow('Frame', self.frame)
	def terminate(self):
		self.cap.release()
		cv2.destroyAllWindows()
