#This program implements the class that utilizes the custom haar cascade
import cv2
import numpy as np
from filters import DetectGear

capture = DetectGear(0)

while(True):
	capture.collectFrames()
	capture.analyze()
	key = cv2.waitKey(1)
	if (key == ord('q')):
		break
capture.terminate()
