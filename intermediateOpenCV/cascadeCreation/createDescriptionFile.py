#This program demonstrates how to create a description file for the negative images for cascade training purposes 
import cv2
import numpy as np
import os

for fileType in ['neg']:
	for img in os.listdir(fileType):

		#Creating a new line for each image
		line = fileType + '/' + img + '\n'

		#Creating a description file; 'open()' requires two arguments: the name of the file to be created and the command; 'a' represents 'append'
		with open('bg.txt', 'a') as f:
			
			#Writing the new line
			f.write(line)
