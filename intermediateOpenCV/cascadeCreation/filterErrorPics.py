#This program demonstrates how to find error pictures and delete them automatically
import cv2
import numpy as np
import os

#Specifies the directory that will be examined
for fileType in ['neg']:

	#Iterates through each image in the specified directory
	for img in os.listdir(fileType):

		#Iterates through the sample error image that was copied/pasted into the 'badFiles' directory
		for badFile in os.listdir('badFiles'):
			try:
				#Indicates the path of the current image
				currentImagePath = str(fileType) + '/' + str(img)

				#Indicates the path of the sample error image
				badFile = cv2.imread('badFiles/' + str(badFile))

				#Reads the current image
				candidateFile = cv2.imread(currentImagePath)

				#The first condition of the if statement ensures that the dimensions of the candidate image and the sample error image are the same
				#The second condition utilizes an XOR gate; XOR only rings true when the two inputs are different; since the XOR is passed through a NOT gate, it will ring true only when the inputs are THE SAME; in this case, when the candidate image matches the sample error image and is therefore an error image!

				#This is also known as an XNOR gate and/or an equivalence gate, since this method can be used to test if two inputs are the same; there are other types, but XNOR is one of the simplest
				if (candidateFile.shape == badFile.shape and not(cv2.bitwise_xor(candidateFile, badFile).any())):
					print("Bad Image identified")
					print(currentImagePath)
					
					#Deletes the current image if it has been identified as an error image
					os.remove(currentImagePath)

			except Exception as e:
				print(str(e))
