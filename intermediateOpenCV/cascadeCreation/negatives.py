#This program demonstrates how to scrape the web for negative images before formatting them for use as negatives in the cascade training process

#urllib is built into python; it allows for web scraping
import urllib.request
import cv2
import numpy as np

#ImageNet is a project to provide images for computer vision research
#This links to a set of pictures of humans; for my cascade, I'm detecting a Gear from FRC Steamworks; it is unlikely any subjects in the photographs will be of yellow plastic gears
#The first set has been commented out

#NEGATIVES_LINK = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n02472987'
NEGATIVES_LINK = 'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07942152'

#Open, read, and properly decode the link
NEGATIVE_URLS = urllib.request.urlopen(NEGATIVES_LINK).read().decode()

#The index counter for each image that will be saved; change it to one above the latest image index if running this program multiple times with other imagesets

#imgIndex = 1
imgIndex = 1470

#The loop cycles through each url; ImageNet splits urls with new lines, so the '.split()' method must be used along with the '\n' flag
for i in NEGATIVE_URLS.split('\n'):
	
	#Since there is a chance that accessing a url could fail, a try/catch system must be used
	try:
		print(i)

		#Retrieves the image url
		urllib.request.urlretrieve(i, "neg/" + str(imgIndex) + ".jpg")

		#Reads the image as grayscale
		img = cv2.imread("neg/" + str(imgIndex) + ".jpg", 0)

		#Resizes the image to 100 x 100: the recommended size for negatives
		img = cv2.resize(img, (100,100))

		#Saves the image in the 'neg' directory
		cv2.imwrite("neg/" + str(imgIndex) + ".jpg", img)

		#Increments the index
		imgIndex +=1
	
	#Exception in case the try fails
	except Exception as e:
		print(str(e))