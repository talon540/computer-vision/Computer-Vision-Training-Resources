#This program demonstrates how to create a class for video analysis
import cv2
import numpy as np

class Capture():
	#Using init to initialize the camera feed
	def __init__(self, camInt):
		self.cap = cv2.VideoCapture(camInt)
	
	#Applying an HSV filter
	def HSV(self, lowerBound, upperBound):
		_, self.frame = self.cap.read()
		hsv = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
		lower = np.array(lowerBound)
		upper = np.array(upperBound)
		mask = cv2.inRange(hsv, lower, upper)
		self.res = cv2.bitwise_and(self.frame, self.frame, mask=mask)
		
		#cv2.imshow('Orig', self.frame)
		#cv2.imshow('Mask', mask)
		#cv2.imshow('Result', self.res)

	#Filtering the result with a Canny Filter and then finding contours
	def Contours(self, minval, maxval):
		greyFrame = cv2.cvtColor(self.res, cv2.COLOR_BGR2GRAY)		
		edges = cv2.Canny(greyFrame, minval, maxval)
		_, self.contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		
		#cv2.imshow('Edges', edges)

	#Filtering contours for the one with the largest area
	def filterContours(self):
		maxArea = 0
		for i in range(0, len(self.contours)):
			if (cv2.contourArea(self.contours[i]) > maxArea):
				maxArea = cv2.contourArea(self.contours[i])
				self.cnt = self.contours[i]

	#Drawing the bounding rectangle of the largest contour
	def drawBound(self):
		x, y, w, h = cv2.boundingRect(self.cnt)
		self.frame = cv2.rectangle(self.frame, (x,y), (x+w,y+h), (255,255,255), 2)
		self.ctr = (x+(w/2), y+(h/2))

		#cv2.imshow('Rectangles', self.frame)

	#Ending the stream
	def terminate(self):
		self.cap.release()
		cv2.destroyAllWindows()
		