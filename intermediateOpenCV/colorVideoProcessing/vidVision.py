#This program utilizes the class that was created
import cv2
import numpy as np
from videoFilters import Capture

#Creating a Capture object with Camera 0
vid = Capture(0)

while(True):
	#The following HSV values and Canny Filter values were used to track a piece of orange paper I had
	vid.HSV((0,220,140), (20,280,240))
	vid.Contours(30, 200)
	vid.filterContours()
	vid.drawBound()

	print(vid.ctr)

	key = cv2.waitKey(1)
	if(key == ord('q')):
		break
vid.terminate()
