#This program demonstrates how to create and set up a video stream for video analysis
import cv2
import numpy as np

#Starts a video capture; the index number inside the parenthesis dictates which camera to use
cp = cv2.VideoCapture(0)

while(True):
	#The '.read()' method outputs two values
	#1. A boolean indicating whether or not the frame was read correctly; useful for debugging
	#2. The frame itself
	ret, frame = cp.read()
	
	#Displaying the frame
	cv2.imshow('Frame', frame)
	
	#Setting 'cv2.waitKey()' to zero would hang the program; it must be nonzero	
	key = cv2.waitKey(1)
	
	#Ending the video capture if the 'q' key is pressed
	if(key == ord('q')):
		break

#'.release()' ends the video capture
cp.release()

cv2.destroyAllWindows()
 