#This program demonstrates how to find contours and basic contour operations in python
import cv2
import numpy as np

#To find contours, noise must first be filtered out

#The original image will be needed for the contour operations, so a different variable must be created for the greyscale image (Canny Edge Detection)
img = cv2.imread('15.jpg')
imgGrey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edge = cv2.Canny(imgGrey, 250, 300)

#In Python, functions and methods can have multiple outputs
#The 'cv2.findContours()' method requires three arguments: source image, contour retrieval mode, and contour approximation method
#The output of the method is a tuple, hence multiple variables are used to store the data
#For the purpose of this program, only 'contours' is needed since it is a list of all the contours
#Each contour is a numpy array of x and y coordinates
image, contours, hierarchy = cv2.findContours(edge, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#The default way of drawing contours with 'cv2.drawContours()' requires five arguments
#1. The image on which to draw upon
#2. List of contours
#3. Which contour to draw (-1 means to draw all of them)
#4. Color of the line
#5. Thickness of the line (in px)
img = cv2.drawContours(img, contours, -1, (0,0,255), 3)

cv2.imshow('Contours', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
