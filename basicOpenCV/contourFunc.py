#This program demonstrates the use of bounding rectangles in OpenCV for target isolation

import cv2
import numpy as np

img = cv2.imread('15.jpg')
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(imgGray, 250, 300)

#Since the image and hierarchy outputs aren't used in this program, an underscore can be used to efficiently drop those data fields
_, contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#Filter out the contours to use only the one with the largest area (ideally this would be the target; this would filter out any remaining noise)
largestArea = 0
for i in range(0, len(contours)):
	if (cv2.contourArea(contours[i]) > largestArea):
		largestArea = cv2.contourArea(contours[i])
		cnt = contours[i]

img = cv2.drawContours(img, cnt, -1, (0,0,255), 2)



#Bounding rectangles can be used to identify key parameters about a target, and the center of a target object can be extrapolated from this data
#'cv2.boundingRect()' requires the contours list as a parameter, and it returns the (x,y) coordinates of the top-left corner of the rectangle along with the width and height
x,y,w,h = cv2.boundingRect(cnt)

#Adding the rectangle to the image
img = cv2.rectangle(img, (x,y), (x+w, y+h), (255,255,255), 3)
#Deriving the center coordinate of the rectangle as a tuple
ctr = ((x+(w/2)),(y+(h/2)))

font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img,
	("Center Coordinate detected at " + str(ctr)),
	(1, 470),
	font,
	0.75,
	(255,255,255),
	2,
	cv2.LINE_AA)

cv2.imshow('Bounding Boxes', img)
cv2.waitKey(0)
cv2.destroyAllWindows()