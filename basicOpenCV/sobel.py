#This program demonstrates how to apply a sobel operator
import cv2
import numpy as np

# A sobel operator performs best when the image is in grayscale
img = cv2.imread('15.jpg', 0)

# 'cv2.Sobel()' requires five inputs
# The source image
# The datatype in which to store data; CV_64F is pretty precise
# Whether or not to conduct the operation on the X-axis
# Whether or not to conduct the operation on the Y-axis
# Kernel Size
sobelX = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)
sobelY = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)

# Apply an absolute value to the sobel operator output from both axes so they can be combined
sobelX_abs = cv2.convertScaleAbs(sobelX_abs)
sobelY_abs = cv2.convertScaleAbs(sobelY_abs)

# Combine the individual axes into one final gradient; each axis is weighted by a half (there are two axes --> 50 / 50 split in contribution)
sobel = cv2.addWeighted(sobelX_abs, 0.5, sobelY_abs, 0.5)

cv2.imshow('Original', img)
cv2.imshow('Sobel', sobel)
cv2.waitKey(0)
cv2.destroyAllWindows()
