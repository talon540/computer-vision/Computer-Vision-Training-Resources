Lesson Sequence
1. Numpy Arrays
2. Image Loading
3. Image Writing
4. Drawing on Images
5. HSV Value Conversion
6. HSV Filter
7. Blur
8. Sobel Operator
9. Canny Edge Detection
10. Contour Detection
11. Contour Functions
12. Cumulative Program