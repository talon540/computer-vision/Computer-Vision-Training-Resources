#This program combines all of the techniques covered so far and applies them to fully analyze a single image

import cv2
import numpy as np

img = cv2.imread('15.jpg')
imgHsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lowerGreen = np.array([81, 100, 100])
upperGreen = np.array([101, 255, 255])
mask = cv2.inRange(imgHsv, lowerGreen, upperGreen)
res = cv2.bitwise_and(img, img, mask=mask)

blur = cv2.GaussianBlur(res, (5, 5), 0)
blur_grey = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)

sobel_x = cv2.Sobel(blur_grey, cv2.CV_64F, 1, 0, ksize=5)
sobel_y = cv2.Sobel(blur_grey, cv2.CV_64F, 0, 1, ksize=5)

sobel_x_abs = cv2.convertScaleAbs(sobel_x_abs)
sobel_y_abs = cv2.convertScaleAbs(sobel_y_abs)

sobel = cv2.addWeighted(sobel_x_abs, 0.5, sobel_y_abs, 0.5)

edges = cv2.Canny(sobel, 225, 300)

_, contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

largestArea = 0
for i in range(0, len(contours)):
	if (cv2.contourArea(contours[i]) > largestArea):
		largestArea = cv2.contourArea(contours[i])
		cnt = contours[i]

x, y, w, h = cv2.boundingRect(cnt)
res = cv2.rectangle(res, (x,y), (x+w,y+h), (255,255,255), 2)
ctr = (x+(w/2),y+(h/2))

font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(
	res,
	("Center Point found at " + str(ctr)),
	(1,470),
	font,
	0.75,
	(255,255,255),
	2,
	cv2.LINE_AA	
	)
cv2.imshow('Result', res)
cv2.waitKey(0)
cv2.destroyAllWindows()
