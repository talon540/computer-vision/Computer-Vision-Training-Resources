#This program demonstrates how to import an image, alter it, and save the altered file as a new image

import cv2
import numpy as np

#Loading a color image as a grayscale image
img = cv2.imread('15.jpg', 0)
cv2.imshow('New Grayscale Image', img)

#By setting a variable equal to 'cv2.waitKey()', the key that is pressed can be logged
key = cv2.waitKey(0)

#27 is the index number of the escape key
if (key == 27):
	cv2.destroyAllWindows()
#'ord()' will determine the index number of a single character, in this case the 's' key so that the image can be saved
elif (key == ord('s')):
	cv2.imwrite('grayGoal.png', img)
	cv2.destroyAllWindows()
