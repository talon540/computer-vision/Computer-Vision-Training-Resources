#This program will convert traditional BGR values into HSV values that match OpenCV's scaling

import cv2
import numpy as np

#sys has a number of system-built functions in python
import sys

#sys.argv allows for the user to directly enter inputs in the command line for use in the code
#NOTE: The first term starts from 1, not 0!
b = sys.argv[1]
g = sys.argv[2]
r = sys.argv[3]

#uint8 is used instead of array so that the interpreter knows to read it as a series of 8-bit integers
color = np.uint8([[[b, g, r]]])
hsvColor = cv2.cvtColor(color, cv2.COLOR_BGR2HSV)

print(hsvColor)