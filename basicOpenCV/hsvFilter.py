#This program demonstrates how to set up a basic HSV filter for analysis

import cv2
import numpy as np

img = cv2.imread('15.jpg')

#Converting the image from BGR color format to HSV color format
hsvImg = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

#For HSV filters, a lower color threshold and an upper color threshold must be defined
#This way, the program will only detect colors within the bounds
#Use an online converter to convert BGR colors to HSV format

#NOTE: Although HSV values can technically be decimals, the library only reads INTEGERS; round all values!
lower = np.array([81, 100, 100])
upper = np.array([101, 255, 255])

#Applying a threshold mask to filter out all the other colors
myMask = cv2.inRange(hsvImg, lower, upper)

#Applying a bitwise conjunction to the original frame and applying a mask
#The Bitwise AND in this case takes the two inputs, which are the same image, and returns itself as the result. HOWEVER, the mask is also applied, and this zeroes any regions of the image that are not active in the mask!
#The reason the image is used for both inputs is that the function requires two inputs
#NOTE: The mask must be defined as 'mask = ...' otherwise the program will interpret it as an input!
final = cv2.bitwise_and(img, img, mask = myMask)


cv2.imshow('Original', img)
cv2.imshow('Mask Image', myMask)
cv2.imshow('Final Image', final)
cv2.waitKey(0)
cv2.destroyAllWindows()
