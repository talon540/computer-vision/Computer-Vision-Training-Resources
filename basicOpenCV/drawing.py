#This program demonstrates the basics of drawing objects within images

import cv2
import numpy as np

img = cv2.imread('15.jpg')

#Adding a rectangle the image; since python is a dynamic language a new variable doesn't have to be created
#To create a rectangle, five parameters must be supplied
#1. Source image
#2. Bottom Left corner coordinate
#3. Top-Right corner coordinate
#4. Color of the rectangle in BGR format
#5. Thickness of the lines in terms of pixels
img = cv2.rectangle(img, (232,196), (375,123), (0,0,255), 2)

#Adding a circle to the center of the rectangle
#To create a circle, five parameters are needed
#1. Source image
#2. Center coordinate
#3. Radius length (in px)
#4. Color in BGR format
#5. Thickness of the lines (in px)
img = cv2.circle(img, (int(round((232+375)/2)),int(round((196+123)/2))), 10, (0,255,0), 2)

#Adding text to the image; its a good practice to declare the font as a variable
font = cv2.FONT_HERSHEY_SIMPLEX
#To add text, eight parameters are needed
#1. Source Image
#2. String
#3. Coordinate
#4. Font
#5. Text Size
#6. Color in BGR format
#7. Thickness of Text (in px)
#8. Line Type
cv2.putText(img, 
	("Central target detected at (" + str((232+375)/2) + "," + str((196+123)/2) + ")"), 
	(1,470), 
	font, 
	0.75, 
	(255,255,255), 
	2, 
	cv2.LINE_AA)

cv2.imshow('Goal', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
