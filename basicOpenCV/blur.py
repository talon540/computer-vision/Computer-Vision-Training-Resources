# This program demonstrates how to apply a Gaussian Blur
import cv2
import numpy as np

img = cv2.imread('15.jpg')

# 'cv2.GaussianBlur()' requires at least three inputs: the source image, a kernel size and the standard deviation along the horizontal axis of the kernal
# Kernel size must be odd so that there is a pixel in the center of the kernel
# Standard Deviation in the X is usually set to 0; this tells the program to dynamically calculate the standard deviation based on the properties of the kernel
# Standard Deviation in the Y can also be given as a fourth input, but only putting the X standard deviation tells the program to assume the Y standard deviation is the same
blur = cv2.GaussianBlur(img, (5,5), 0)

cv2.imshow('Original', img)
cv2.imshow('Blur', blur)
cv2.waitKey(0)
cv2.destroyAllWindows()
