#This program demonstrates how to use Canny Edge Detection to filter out noise from an image
import cv2
import numpy as np

#For an image to be processed by canny edge detection, it must be in grayscale
img = cv2.imread('15.jpg', 0)


#'cv2.Canny()' requires at least three inputs: the source image, a minimum value and a maximum value
#Minimum value determines the intensity gradient at which a collection of pixels is deemed not to be an edge
#Maximum value determines the intensity gradient at which a collection of pixels is deemed to definitely be an edge
#For pixels in between the min and max vals, the Canny Detection looks to see if these pixels are connected to any other pixels that exceed the max val
#If the pixels are connected to other pixels that exceed the max val, it is deemed an edge; if not, it is deemed as noise
edges = cv2.Canny(img, 240, 300)

cv2.imshow('Original', img)
cv2.imshow('Edges', edges)
cv2.waitKey(0)
cv2.destroyAllWindows()
 
