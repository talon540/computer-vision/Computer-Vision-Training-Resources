#This program demonstrates how to load an image using OpenCV

import cv2
import numpy as np

#Loading an image
img = cv2.imread('15.jpg')

#Showing an image; two parameters are required for the 'cv2.imshow()' command: The title of the window that will be generated and the image to be shown
cv2.imshow('Image of the Goal', img)

#'cv2.waitKey()' sets the time (in milliseconds) that the system waits for a keypress; 
#setting it to zero means that the computer waits for an indefinite time; 
#after a keypress is detected the program runs the subsequent lines
cv2.waitKey(0)

#'cv2.destroyAllWindows()' closes all open windows
cv2.destroyAllWindows()

#Loading and showing an image in grayscale; notice the '0' as the second parameter of 'cv2.imread()'
img2 = cv2.imread('15.jpg', 0)
cv2.imshow('Grayscale image of the goal', img2)
cv2.waitKey(0)
cv2.destroyAllWindows()