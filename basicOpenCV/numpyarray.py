#This program demonstrates basic use of numpy arrays
import numpy as np

#Numpy arrays have been shown to provide higher speed performance compared to traditional python arrays; numpy arrays have also been shown to use less memory

#Essentially, its better to use numpy arrays in situations where big data must be handled; the benefits of numpy arrays are very apparent in those cases

#Creating an array
array1 = np.array([1,2,3,4])
array2 = np.array([5,6,7,8])

#Adding the array values; this only adds array values with the same index
print(array1 + array2)

#Multidimensional array
array3 = np.array([[1,2],[2,4],[3,6]])
array4 = np.array([[4,8],[5,10],[6,12]])

#Adding multidimensional arrays; again, it only adds values with the same index
print(array3 + array4)

#Multiplying arrays
print(array1*array2)

print(array3*array4)
