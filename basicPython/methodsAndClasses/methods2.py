#This program demonstrates how to set up reference files from which classes can be imported

#In a reference file, only the classes and their methods must be defined; no actual scripts should be written unless they need to be run when the class is imported

class Player():
	def __init__(self, name, skill):
	#Other attributes can be defined for initialization so long as they use constant parameters	
		self.hp = 100
		self.mana = 100
		
		self.name = name
		self.skill = skill

	def attack(self, target):
		print(self.name + " attacked " + target)
class Enemy():
	def __init__(self, enemyType, level):
		self.hp = 100
		self.mana = 100
		self.enemyType = enemyType
		self.level = level
	def attack(self, target):
		print(self.enemyType + " attacked " + target)
	