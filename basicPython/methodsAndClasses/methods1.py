#This program demonstrates the difference between functions and methods, and it also introduces classes/objects

#This is a traditional function; it can be run without any special syntax
def add(*args):
	summation = 0
	for i in args:
		summation += i
	return summation
print(add(5, 6, 11))

#Class creation in python allows for the use of object-oriented programming
class Human():
	#Methods in python are functions that are contained within classes
	#ALL Methods require the first parameter to be 'self'; this is so that Python knows to attribute the results of the method to the OBJECT ITSELF
	
	#__init__ is the initialization method; this is always run when the class is called by the program
	def __init__(self, name, age, gender):
		self.name = name
		self.age = age
		self.gender = gender
	def myNameIs(self):
		print("My name is " + self.name)
	
	#Traditional functions may be included within methods
	def addition(self, *args):
	    #Notice how '*' still has to be used for '*args'; this is so that all of the inputs are passed from addition() to add()
		print(self.name + " added some numbers to get " + str(add(*args)))

#Initializing an object based on the class; notice how 'self' is never specified because it is IMPLICIT
norman = Human("Norman", "30", "Male")

#The method myNameIs() can be called as an attribute of the object
norman.myNameIs()

norman.addition(5, 4, 3)