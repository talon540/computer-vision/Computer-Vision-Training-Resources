#This program demonstrates how to import classes from other reference files

#It is generally a good practice to use 'from ... import' so that only the necessary utilities are imported; this reduces memory use
from methods2 import Player
from methods2 import Enemy

player1 = Player("Arne Darvin", "Trickster")
enemy1 = Enemy("Orc", "1")

print("Welcome " + player1.name + ", you have " + str(player1.hp) + " health and " + str(player1.mana) + " mana")

player1.attack(enemy1.enemyType)

enemy1.attack(player1.name)