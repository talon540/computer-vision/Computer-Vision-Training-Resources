#This program demonstrates basic array operations in python

#Creating a list of integers
myList = [1, 2, 3, 4, 5]

#Prints the first term of the list; notice how the index starts from 0
print(myList[0])

#Prints a sublist from the starting index value to up till but not including the final index value
#This will print a sublist of the first four terms
print(myList[0:4])

#Creating a new list out of a sublist
myList2 = myList[0:3]
print(myList2)

#Adding a value to the list can be achieved with the '.append()' function
myList.append(6)
print(myList)

#Deriving the length of the list can be achieved using the 'len()' function
print(len(myList))

#Adding a value to a specific location in the array can be achieved with the '.insert()' command
#The value will be inserted before the specified index value
myList.insert(0, 'Hello')
print(myList)

#Since the original first term's index value will have shifted up by one, this will place 'There' after 'Hello' in the list
myList.insert(1, 'There')
print(myList)

#Adding a value into the middle of the list
#In this case the index value must be rounded to convert it from a float into an integer
myList.insert(round((len(myList))/2), 88)
print(myList)