#The following program demonstrates basic function creation and utilization in python

#Functions can be declared using 'def' followed by the name of the function
#Inputs can be specified within the parenthesis and used locally within the function

#This function checks if a number is prime, by seeing if it is divisible by whole numbers using modulus
def primes(num):
	primeStat = True
	for i in range(2, num):
		if (num % i == 0):
			primeStat = False
	return primeStat

#Call the function by typing it in while also giving it an input
if (primes(4) == True):
	print("I think this function is broken... how can 4 be prime?")
else:
	print("I think it's working, 4 is composite")
if (primes(7) == True):
	print("I'm pretty sure 7 is a prime number, so it must be working")
else:
	print("Well drat")

#Functions can have multiple outputs
def divide(a, b):
	c = a / b
	d = a % b
	#The value that is returned is a tuple -> (c,d)
	return c,d
#x -> c; y -> d
x, y = divide(5, 4)
print(x,y)
print("The quotient is " + str(x))
print("The remainder is " + str(y))
		