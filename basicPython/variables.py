#This program demonstrates the common datatypes in python and how to output them.

#Integers are any nondecimal value
myInt = 16

#Strings are an array of multiple characters
myCity = "England"

#Double/Decimal are rational numbers
myDec = 3.14

#Booleans can be either True or False
#NOTE: In Python, you must capitalize the first letter of True and False; THIS IS DIFFERENT FROM JAVA OR C.
myBool = True

#To Print an integer as part of a string, you must parse it into a string. This can be done with the 'str()' command.
print("I have " + str(myInt) + " gigs of RAM")

#If you are just printing the integer, you don't need to parse it.
print(myInt)

#You don't need to do any special parsing to insert a string as part of a larger string, since the variable is already a string.
print(myCity + " is my City.")

#The same parsing method must be used for decimal/double values if you wish to print it as part of a string.
print("Pi is roughly " + str(myDec))

#Like integers, decimal values do not need to be parsed if they are the only value being printed.
print(myDec)

#Booleans must also be parsed if they are to be a part of a string
print("The condition is " + str(myBool))

#Like integers and decimals, booleans don't need to be parsed if they are the only value being printed
print(myBool)