#This program demonstrates basic bitwise functions in python
import sys as s

a = int(s.argv[1])
b = int(s.argv[2])

#Bitwise AND function
#Bitwise breaks down the integer into its binary number
#Bitwise AND will multiply corresponding digits of two binary numbers to get a result
# 3 -> 0011
# 2 -> 0010
# R -> 0010 -> 2

c = a & b
print(c)

#Bitwise OR Function
#Bitwise OR will look to see if one of the digits is 1
# 3 -> 0011
# 2 -> 0010
# R -> 0011 -> 3

d = int(s.argv[3])
e = int(s.argv[4])
f = d | e
print(f)

#Bitwise XOR Function
#Bitwise XOR will output 1 if and only if two digits are not the same
# 3 -> 0011
# 2 -> 0010
# R -> 0001 -> 1

# 3 -> 0011
# 3 -> 0011
# R -> 0000 -> 0
#NOTE: If zero is the output of a Bitwise XOR; that means that the two inputs must be the same

g = int(s.argv[5])
h = int(s.argv[6])
i = g ^ h
print(i)

