LESSON SEQUENCE
1. Hello World
2. Variables
3. Operands
4. Loops
5. Arrays
6. Functions
7. Arguments
8. Bitwise Functions
9. Methods and Classes