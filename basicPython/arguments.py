#The following program demonstrates the use of *args and **kwargs in python

#*args allows for a function to accept any number of inputs

def summation(*args):
	mySum = 0
	#This line will iterate through all of the inputs that are provided
	for i in args:
		mySum += i
	print("The sum is " + str(mySum))

summation(5, 4, 0)
summation(1, 2, 3, 88, 99, 23284)

#**kwargs allows for the passing of any number of keyword arguments (a value with an identifier)
def lister(**kwargs):
	#i corresponds to the keyword; j corresponds to the actual value
	#Like *args, this for loop will iterate through all the keyword arguments that are inputted
	for i, j in kwargs.items():
		print(i,j)
#Notice how each value is first given a keyword (i -> name; j -> "John Doe")
lister(name = "John Doe", age = 47, favoriteFood = "Stromboli")

lister(ruler = "Julius Caesar", empire = "Rome", killer = "A bunch of triggered senators", successor = "Augustus")

#Static inputs, *args, and **kwargs can be combined if necessary
#Python will recognize if an item is an argument or a keyword argument, since a regular argument lacks a keyword/identifier
def blogPost(title, *args, **kwargs):
	print(title)
	totalViews = 0
	for a in args:
		totalViews += a
	print("There are " + str(totalViews) + " total views")
	for b,c in kwargs.items():
		print (b, c)
#Long lines can be broken up and indented; Python will read it as a single line
#In this example, let the arguments (*args) be the number of views in some random period of time
blogPost("My Blog", 
	5, 
	4, 
	0, 
	subscriberCount = -1, 
	follows = 0, 
	blog_post_1 = "Why I think Java is over-rated.")

#The number of arguments can be derived using the 'len()' function
def average(*args):
	s = 0
	for i in args:
		s += i
	print("There are " + str(len(args)) + " numbers")
	avg = s / len(args)
	print("The average is " + str(avg))
average(5,4)
average(1, 5, 10)